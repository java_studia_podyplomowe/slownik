package diki;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReaderWriterDiki implements ObslugaSlownika{
    private List<Word> slowka;

    public List<Word> getSlowka() {
        return slowka;
    }

    public void setSlowka(List<Word> slowka) {
        this.slowka = slowka;
    }

    public ReaderWriterDiki(List<Word> slowka) {
        this.slowka = slowka;
    }

    public ReaderWriterDiki() {
        this.slowka = new ArrayList<>();
    }

    public List<Word> readFile(String path) throws FileNotFoundException, NotValidLineFormatException {
        File file = new File(path);
        Scanner in = new Scanner(file);
        while(in.hasNext()){
            slowka.add(Mapper.toWord(in.nextLine()));
        }
        return slowka;
    }

    @Override
    public String toString() {
        String toPrint = "Nasz słownik:\n";
        for (Word linia: slowka){
            toPrint = toPrint+linia+"\n";
        }
        return toPrint;
    }

    // *************
    // Do zrobiena!!!!
    public boolean writeFile(String path){return false;}
}
