package diki;

public class Mapper {
    private Mapper() {
    }
    public static Word toWordBasic(String linia){
        String angWord = linia.substring(0, linia.indexOf("[")).trim();
        linia = linia.substring(linia.indexOf("["), linia.length()).trim();
        String wymowa = linia.substring(linia.indexOf("[")+1, linia.indexOf("]")-1);
        linia = linia.substring(linia.indexOf("["), linia.length()).trim();
        String znczenie = linia.substring(linia.indexOf("]")+1, linia.length());
        return new Word(angWord, wymowa, znczenie);
    }
    static Word toWord(String line) throws NotValidLineFormatException{
        int firstSplitPosition = line.indexOf("[");
        int secondSplitPosition = line.indexOf("]");
        if (firstSplitPosition==-1 || secondSplitPosition==-1){
            throw new NotValidLineFormatException("Nieprawidłowy format linii");
        }
        String angSlowo = line.substring(0, firstSplitPosition).trim();
        String wymowa = line.substring(firstSplitPosition, secondSplitPosition+1).trim();
        String wyjasnienie = line.substring(secondSplitPosition+1, line.length()).trim();
        if (angSlowo.length()<1 || wymowa.length()<1 || wymowa.length()<1){
            throw new NotValidLineFormatException("Brakuje słowa, wymowy lub definicji");
        }
        return new Word(angSlowo, wymowa, wyjasnienie);
    }
}
