package diki;

public class NotValidLineFormatException extends Exception{
    public NotValidLineFormatException() {
    }
    public NotValidLineFormatException(String nieprawidłowy_format_linii) {
        super(nieprawidłowy_format_linii);
    }
}
