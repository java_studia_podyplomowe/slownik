package diki;

import java.util.ArrayList;
import java.util.List;

public class Slownik {
    List<Word> slowa;

    public Slownik(List<Word> slowa) {
        this.slowa = slowa;
    }
    public Slownik() {
        this.slowa = new ArrayList<>();
    }

    public boolean addWord(Word word){
        if(word.equals("") || word==null){
            return false;
        }
        return slowa.add(word);
    }

    public Word searchWord(String angWord){
        for(Word word:slowa){
            if(word.getAngWord().equals(angWord))
                return word;
        }
        return null;
    }
}
