package diki;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ReaderWriterDikiByMap implements ObslugaSlownikaByMap{
    Map<String, Word> slowka;

    public Map<String, Word> getSlowka() {
        return slowka;
    }

    public void setSlowka(Map<String, Word> slowka) {
        this.slowka = slowka;
    }

    public ReaderWriterDikiByMap() {
        this.slowka = new HashMap<>();
    }

    public Map<String,Word> readFile(String path) throws FileNotFoundException, NotValidLineFormatException {
        File file = new File(path);
        Scanner in = new Scanner(file);
        while(in.hasNext()){
            Word word = Mapper.toWord(in.nextLine());
            slowka.put(word.getAngWord(),word);
        }
        return slowka;
    }

    @Override
    public String toString() {
        String toPrint = "Nasz słownik:\n";
        for (Word linia: slowka.values()){
            toPrint = toPrint+linia+"\n";
        }
        return toPrint;
    }

    // *************
    // Do zrobiena!!!!
    public boolean writeFile(String path){return false;}
}
