package diki;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SlownnikByMap {
    Map<String, Word> slowa;

    public SlownnikByMap(Map<String, Word> slowa) {
        this.slowa = slowa;
    }

    public SlownnikByMap() {
        slowa = new HashMap<>();
    }

    public Map<String, Word> getSlowa() {
        return slowa;
    }

    public void setSlowa(Map<String, Word> slowa) {
        this.slowa = slowa;
    }

    public Word addWord(Word word){
        if(word.equals("") || word==null){
            return null;
        }
        return slowa.put(word.getAngWord(), word);
    }

    public Word searchWord(String angWord){
        return slowa.get(angWord);
    }
}
