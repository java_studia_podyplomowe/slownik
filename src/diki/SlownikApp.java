package diki;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class SlownikApp {
    public static void main(String[] args) throws FileNotFoundException, NotValidLineFormatException {
        String path ="C:\\Users\\zosia\\Desktop\\diki.txt";
        String doTlumaczenia = (new Scanner(System.in)).nextLine();
        //Lista
        ReaderWriterDiki readerWriterDiki = new ReaderWriterDiki();
        Slownik slownik = new Slownik(readerWriterDiki.readFile(path));
        System.out.println(slownik.searchWord(doTlumaczenia));
        //Mapa
        ReaderWriterDikiByMap readerWriterDiki2 = new ReaderWriterDikiByMap();
        SlownnikByMap slownik2 = new SlownnikByMap(readerWriterDiki2.readFile(path));
        System.out.println(slownik2.searchWord(doTlumaczenia));


    }
}
