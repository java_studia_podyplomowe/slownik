package diki;

import java.io.FileNotFoundException;
import java.util.List;

public interface ObslugaSlownika {
    List<Word> readFile(String path) throws FileNotFoundException, NotValidLineFormatException;
    boolean writeFile(String path);
}
