package diki;

public class Word {
    private String angWord;
    private String wymowa;
    private String wyjasnienie;

    public Word(String angWord, String wymowa, String wyjasnienie) {
        this.angWord = angWord;
        this.wymowa = wymowa;
        this.wyjasnienie = wyjasnienie;
    }

    public Word(String angWord, String wyjasnienie) {
        this.angWord = angWord;
        this.wymowa = "";
        this.wyjasnienie = wyjasnienie;
    }

    @Override
    public String toString() {
        return angWord + ' ' + wymowa + ' ' + wyjasnienie;
    }

    public String getAngWord() {
        return angWord;
    }

    public void setAngWord(String angWord) {
        this.angWord = angWord;
    }

    public String getWymowa() {
        return wymowa;
    }

    public void setWymowa(String wymowa) {
        this.wymowa = wymowa;
    }

    public String getWyjasnienie() {
        return wyjasnienie;
    }

    public void setWyjasnienie(String wyjasnienie) {
        this.wyjasnienie = wyjasnienie;
    }
}
