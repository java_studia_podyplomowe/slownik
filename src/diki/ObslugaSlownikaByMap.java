package diki;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

public interface ObslugaSlownikaByMap {
    Map<String, Word> readFile(String path) throws FileNotFoundException, NotValidLineFormatException;
    boolean writeFile(String path);
}
